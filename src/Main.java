import Model.ClientEntity;
import Model.ComandaEntity;
import Model.EmpEntity;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import java.math.BigInteger;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static connection.ConnexioEmpresa.getSession;

public class Main {


    public static void main(final String[] args) throws Exception {
        final Session session = getSession();
//        ex1(session);
//        ex2(session);
//        ex3(session);
//        ex4(session);
//        ex5(session);
//        ex6(session);
        ex7(session);
        session.close();
    }

    /**
     * Exercici 1. Mostra tota la informació de totes les comandes.
     * @param session Session
     */
    public static void ex1(Session session) {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cr = cb.createQuery(ComandaEntity.class);
        Root<ComandaEntity> root = cr.from(ComandaEntity.class);
        cr.select(root);

        Query<ComandaEntity> query = session.createQuery(cr);
        List<ComandaEntity> results = query.getResultList();

        System.out.println("COMANDA:");
        for ( ComandaEntity r : results ) {
            String tipus = (r.getComTipus() == null) ? "null" : r.getComTipus();
            System.out.printf("[NUM: %d]\t[DATA: %s]\t[TIPUS: %s]\t[CLIENT_COD: %d]\t[DATA_TRAMESA: %s]\t[TOTAL: %.2f]\n",
                    r.getComNum(), r.getComData().toString(), tipus, r.getClientByClientCod().getClientCod(), r.getDataTramesa().toString(), r.getTotal());
        }
    }

    /**
     * Exercici 2. Mostra el nom del client (client_cod) i la data de comanda (com_data) de les comandes que el camp
     * com_tipus sigui null
     * @param session Session
     */
    public static void ex2(Session session) {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cr = cb.createQuery(ComandaEntity.class);
        Root<ComandaEntity> root = cr.from(ComandaEntity.class);
        cr.select(root).where(cb.isNull(root.get("comTipus")));

        Query<ComandaEntity> query = session.createQuery(cr);
        List<ComandaEntity> results = query.getResultList();

        System.out.println("COMANDA:");
        for ( ComandaEntity r : results ) {
            String tipus = (r.getComTipus() == null) ? "null" : r.getComTipus();
            System.out.printf("[CLIENT_COD: %d]\t[DATA: %s]\t[TIPUS: %s]\n",
                    r.getClientByClientCod().getClientCod(), r.getDataTramesa().toString(), tipus);
        }
    }

    /**
     * Exercici 3. Mostra el nom i el telèfon del clients de la ciutat de BURLINGAME
     * @param session Session
     */
    public static void ex3(Session session) {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> cr = cb.createQuery(ClientEntity.class);
        Root<ClientEntity> root = cr.from(ClientEntity.class);
        cr.select(root).where(cb.like(root.get("ciutat"), "BURLINGAME"));

        // SELECT nom, telefon FROM client WHERE cuitat = 'BURLINGAME';

        Query<ClientEntity> query = session.createQuery(cr);
        List<ClientEntity> results = query.getResultList();

        System.out.println("CLIENT:");
        for ( ClientEntity r : results ) {
            System.out.printf("[NOM: %s]\t[TELEFON: %s]\t[CIUTAT: %s]\n", r.getNom(), r.getTelefon(), r.getCiutat());
        }
    }

    /**
     * Exercici 4. Mostra tota la informació de l’empleat amb cognom CEREZO
     * @param session Session
     */
    public static void ex4(Session session) {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EmpEntity> cr = cb.createQuery(EmpEntity.class);
        Root<EmpEntity> root = cr.from(EmpEntity.class);
        cr.select(root).where(cb.like(root.get("cognom"), "CEREZO"));

        Query<EmpEntity> query = session.createQuery(cr);
        List<EmpEntity> results = query.getResultList();

        System.out.println("EMP:");
        for ( EmpEntity r : results ) {

            System.out.printf("[EMP_NO: %d]\t[COGNOM: %s]\t[OFICI: %s]\t[CAP: %s]\t[DATA_ALTA: %s]\t[SALARI: %d]\t[COMISSIO: %d]\t[DEPT_NO: %d]\n",
                    r.getEmpNo(), r.getCognom(), r.getOfici(), r.getEmpByCap().getEmpByCap(), r.getDataAlta(), r.getSalari(), r.getComissio(), r.getDeptByDeptNo().getDeptNo());
        }
    }

    /**
     * Exercici 5. Mostra el codi (com_num) de les comandes que la data de tramesa sigui entre el 1 de gener del 1986 i el
     * 30 de setembre del 1986. Utilitza el CriteriaBuilder.between.
     * @param session Session
     */
    public static void ex5(Session session) {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<ComandaEntity> cr = cb.createQuery(ComandaEntity.class);
        Root<ComandaEntity> root = cr.from(ComandaEntity.class);
        cr.select(root).where(cb.between(root.get("dataTramesa"), Date.valueOf("1986-01-01"), Date.valueOf("1986-09-30")));

        Query<ComandaEntity> query = session.createQuery(cr);
        List<ComandaEntity> results = query.getResultList();

        System.out.println("COMANDA:");
        for ( ComandaEntity r : results ) {
            System.out.printf("[CLIENT_COD: %d]\t[DATA: %s]\n",
                    r.getClientByClientCod().getClientCod(), r.getDataTramesa().toString());
        }
    }

    /**
     * Excercici 6. Mostra el cognom i l’ofici dels empleats que cobrin més de 300.000.
     * @param session Session
     */
    public static void ex6(Session session) {

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EmpEntity> cr = cb.createQuery(EmpEntity.class);
        Root<EmpEntity> root = cr.from(EmpEntity.class);
        cr.select(root).where(cb.gt(root.get("salari"), 300000));

        Query<EmpEntity> query = session.createQuery(cr);
        List<EmpEntity> results = query.getResultList();

        System.out.println("EMP:");
        for ( EmpEntity r : results ) {

            System.out.printf("[COGNOM: %s]\t[OFICI: %s]\t[SALARI: %d]\n",
                    r.getCognom(), r.getOfici(), r.getSalari());
        }
    }

    /**
     * Excercici 7. Mostra quants empleats té cada departament. Utilitza el CriteriaQuery.groupBy
     * @param session Session
     */
    public static void ex7(Session session) {

//        CriteriaBuilder cb = session.getCriteriaBuilder();
//        CriteriaQuery<Long> cr = cb.createQuery(Long.class);
//        Root<EmpEntity> root = cr.from(EmpEntity.class);
//
//        // SELECT dept_no, COUNT(*) FROM emp GROUP BY dept_no;
////        cr.multiselect(root.get("deptByDeptNo"), cb.count(root.get("empNo"))).groupBy(root.get("deptByDeptNo")); // (???)
//        cr.select(cb.count(root.get("empNo"))).groupBy(root.get("deptByDeptNo"));
//
//        Query<Long> query = session.createQuery(cr);
//        List<Long> results = query.getResultList();
//
//        System.out.println("EMP:");
//        for ( Long e : results ) {
//            System.out.println(e);
//        }

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Tuple> cr = cb.createQuery(Tuple.class);
        Root<EmpEntity> root = cr.from(EmpEntity.class);

        // SELECT dept_no, COUNT(*) FROM emp GROUP BY dept_no;
        cr.multiselect(root.get("deptByDeptNo").get("deptNo"), cb.count(root.get("empNo"))).groupBy(root.get("deptByDeptNo").get("deptNo"));

        Query<Tuple> query = session.createQuery(cr);
        List<Tuple> results = query.getResultList();

        System.out.println("EMP:");
        for ( Tuple t : results ) {
            System.out.printf("DEPT: %d, COUNT: %d\n", (BigInteger) t.get(0), (Long) t.get(1));
        }
    }
}